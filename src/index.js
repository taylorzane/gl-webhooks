import express from 'express'
import bodyParser from 'body-parser'

const DEFAULTS = {
  token: undefined,
  port: 4400,
  branches: ['master'],
  events: ['push'],
  on: {
    receive (json) {
      console.log(json)
    }
  }
}

const fail = (msg, code = 1) => {
  console.error(msg)
  process.exit(code)
}

class GLWebhook {
  constructor (options) {
    this.app = express()
    this.app.use(bodyParser.json())

    this.app.post('/', (req, res) => {
      const body = req.body

      const isValid = this.validate(req)

      if (isValid) {
        this.config.on.receive(body)
        res.status(200).end()
      } else {
        console.log(req.body)
        res.status(400).send('Invalid Request')
      }
    })

    this.config = Object.assign({}, DEFAULTS, options)
  }

  listen (ready) {
    if (this.config.port) {
      this.app.listen(this.config.port, () => { ready() })
    } else {
      fail('Port was not set!')
    }
  }

  validate (req) {
    const json = req.body
    const token = req.headers['x-gitlab-token']

    let valid = false
    if (json) {
      const event = json.object_kind
      const branch = (() => {
        switch (event) {
          case 'push':
            return json.ref.match(/refs\/heads\/(.+?)$/)[1]
          case 'merge_request':
            return json.object_attributes.target_branch
          case 'pipeline':
            return json.object_attributes.ref
          default:
            return null
        }
      })()

      valid =
        this.config.token === token &&
        (
          this.config.events.includes(event) ||
          this.config.events.length === 0 ||
          this.config.events === null ||
          this.config.events === undefined
        ) &&
        (
          this.config.branches.includes(branch) ||
          this.config.branches.length === 0 ||
          this.config.branches === null ||
          this.config.branches === undefined
        )

      return valid
    } else {
      return valid
    }
  }
}

export default GLWebhook
