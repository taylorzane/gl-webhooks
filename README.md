# gl-webhooks

This project lets you easily run JavaScript code in response to a GitLab webhook.

This project currently supports the following event types:

- `push`
  - This occurs when a branch is pushed to.
- `merge_request`
  - This occurs when a merge request is made, the branch it gets triggered on is the branch that the merge request is merging into.
- `pipeline`
  - This occurs when a pipeline's (build) state changes.
  - TOOD: Add handling to specify the state to trigger on. (i.e. `build`, `test`, `deploy`)

For more information on GitLab Webhooks, visit: [Webhooks](https://gitlab.com/help/web_hooks/web_hooks)

# Getting started

## Defaults
```javascript
{
  token: undefined, // {String} : GitLab "Secret Token" field.
  port: 4400, // {Number} : Listen port.
  branches: ['master'], // {Array{String}} : Branches to trigger on.
  events: ['push'], // {Array{String}} : Events to trigger on.
  /*
  NOTE: If branches or events is set to null, undefined, or [] it will trigger on all webhooks.
  */
  on: { // {Object} : Object that holds all lifecycle events (Currently just receive).
    receive (json) { // {Function} : Executes if token, branch, and event matches.
      console.log(json)
    }
  }
}
```

## ES2015/ES6
```javascript
import GLWebhooks from 'gl-webhooks'

// NOTE: All options are optional.
// NOTE: These are the defaults.
const options = {
  token: undefined,
  port: 4400,
  branches: ['master'],
  events: ['push'],
  on: {
    receive (json) {
      console.log(json)
    }
  }
}

const glWebhook = new GLWebhooks(options)

glWebhook.listen(() => {
  // NOTE: This function is called after the server is ready to accept connections.
  console.log('Ready...')
})
```

## ES5
```javascript
var GLWebhooks = require('gl-webhooks').default

// NOTE: All options are optional.
// NOTE: These are the defaults.
var options = {
  token: undefined,
  port: 4400,
  branches: ['master'],
  events: ['push'],
  on: {
    receive: function (json) {
      console.log(json)
    }
  }
}

var glWebhook = new GLWebhooks(options)

glWebhook.listen(function () {
  // NOTE: This function is called after the server is ready to accept connections.
  console.log('Ready...')
})
```

# TODO
  - [ ] Update the library to support ES5, without needing `require().default`.
  - [ ] Add handling to specify the state to trigger on. (i.e. `build`, `test`, `deploy`)

# License

[BSD 2-clause “Simplified” License](LICENSE)
